# Latex Builder
This repo contains a Nix Flake that can be used to build LaTeX PDFs in completely  reproducible ways -- i.e. this will produce an identical output PDF for any given ( and valid ) input configuration.
## Usage
If you would like to use this project, you have a couple of options of how to do so:
### Static PDF w/o Configuation
 - If you want to compile a completely static PDF such that someone with the exact same `document.tex` could locally compile to the exact same PDF, all  you would need to do is run the following command in the directory containing your your TeX file, which must be named `document.tex` ( assuming that you have nix configured to allow the use of flakes, of course ):
  - `nix run gitlab:reproducible-latex/latex-builder.git`
### Static PDF w/ Configuration
 - If you would like to change some meta-parameters about how the PDF is created -- e.g. whether the timestamp is set at compile time or last-edit -- you can set the following flags when you call the previous command:
  - TODO
### Dynamic PDF
 - TODO
## Inspiration
The list below is an incomplete record of sources of inspiration for the contents of this repo -- it will likely grow:
### [flyx](https://flyx.org/)
 - Nearly the entirety of this repo is a result of following along with [this tutorial / article](https://flyx.org/nix-flakes-latex/)  presented by flyx on the eponymous website.
## License
This repository and its contents are licensed under [this license](LICENSE.txt).
